Name:           libnetfilter_cttimeout
Version:        1.0.1
Release:        1
Summary:        Netfilter/conntrack Timeout policy tuning
License:        GPLv2+
URL:            http://netfilter.org
Source0:        http://netfilter.org/projects/%{name}/files/%{name}-%{version}.tar.bz2
BuildRequires:  libmnl-devel >= 1.0.0 pkgconfig kernel-headers gcc
BuildRequires:  make

%description
libnetfilter_cttimeout is the userspace library that provides the programming
interface to the fine-grain connection tracking timeout infrastructure. With
this library, you can create, update and delete timeout policies that can be
attached to traffic flows. This library is used by conntrack-tools

%package        devel
Summary:        Development files for the libnetfilter_cttimeout library
Requires:       %{name} = %{version}-%{release} libmnl-devel >= 1.0.0 kernel-headers

%description    devel
The libnetfilter_cttimeout-devel package includes header files and libraries necessary
for the libnetfilter_cttimeout library.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc COPYING README
%{_libdir}/*.so.*

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/libnetfilter_cttimeout
%{_includedir}/libnetfilter_cttimeout/*.h

%changelog
* Tue Feb 07 2023 xingwei <xingwei14@h-partners.com> - 1.0.1-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update libnetfilter_cttimeout to 1.0.1

* Tue Nov 01 2022 xingwei <xingwei14@h-partners.com> - 1.0.0-15
- add make as BuildRequires for rpmbuild

* Mon Jun 21 2021 yanglu<yanglu72@huawei.com> - 1.0.0-14
- DESC: add buildrequire gcc

* Tue Dec 31 2019 gulining<gulining1@huawei.com> - 1.0.0-13
- Pakcage init
